#pragma once

#include <iostream>
#include <string>

using namespace std;

class Course
{
private:
	//string* _name;
	string _name;
	int _test1;
	int _test2;
	int _exam;

public:
	void init(string name, int test1, int test2, int exam);
	string getName();
	int* getGrades();
	double getFinalGrade();
	void printInfo();
};
