#include "Student.h"

void Student::init(string name, Course** courses, int crsCount)
{
	_name = name;

	if (_courses != NULL)
		delete [] _courses;
		
	_crsCount = crsCount;
	_courses = *courses;
}

string Student::getName()
{
	return _name;
}

void Student::setName(string name)
{
	_name = name;
}
double Student::getAvg()
{
	double avg = 0;
	for (int i = 0; i < _crsCount; i++)
		avg += _courses[i].getFinalGrade();
	return avg / _crsCount;
}

int Student::getCrsCount()
{
	return _crsCount;
}

Course** Student::getCourses()
{
	return &_courses;
}

void Student::resetStudent()
{
	delete [] _courses;
}