#include "Course.h"


void Course::init(string name, int test1, int test2, int exam)
{
	//_name = new string(name);
	_name = name;
	_test1 = test1;
	_test2 = test2;
	_exam = exam;
}

string Course::getName()
{
	//return *_name;
	return _name;
}

int* Course::getGrades()
{
	int *list = new int[3];
	list[0] = _test1;
	list[1] = _test2;
	list[2] = _exam;
	return list;
}

double Course::getFinalGrade()
{
	return 0.25*_test1 + 0.25*_test2 + 0.5*_exam;
}

void Course::printInfo()
{
//	cout << "Course name: " << *_name << endl;
	cout << "Course name: " << _name << endl;
	cout << "Test1 grade: " << _test1 << endl;
	cout << "Test2 grade: " << _test2 << endl;
	cout << "Exam grade: " << _exam << endl;
	cout << "Course grade: " << 0.25*_test1 + 0.25*_test2 + 0.5*_exam << endl;
}