#include "Student.h"

void printMenu();
int getSelection();
Course* getCourses(int crsCount);

void main()
{
	int selection = 0, crsCount = 0;
	string name;
	Student student;
	Course* courses;

	do
	{
		printMenu();
		selection = getSelection();
		switch (selection)
		{
		case 1:

			// Student name
			cout << "Enter student name: ";
			cin >> name;
			student.setName(name);

			// Courses amount
			cout << "How many courses does " << name << " go to? ";
			cin >> crsCount;

			// Courses
			courses = getCourses(crsCount); // Getting courses from function
			
			// Initializing student
			student.init(name, &courses, crsCount);
			break;

		case 2: // Print all courses average
			cout << student.getAvg() << endl;
			break;

		case 3: // Print student name and data about each course.
			if (name != "")
				cout << "Student name: " << student.getName() << endl;
			if (crsCount > 0)
				for (int i = 0; i < crsCount; i++)
					(*student.getCourses())[i].printInfo();
				cout << endl;
			break;

		case 4: // Exit (remember to free data)
			cout << "Good bye!" << endl;
			student.resetStudent();
			break;

		default:
			cout << "Invalid selection!" << endl;
		}
	} while (selection != 4);
}

void printMenu()
{
	cout << "1 - Enter student details\n";
	cout << "2 - Print total average of all courses\n";
	cout << "3 - Print everything\n";
	cout << "4 - Exit\n" << endl;

}
int getSelection()
{
	cout << "Enter selection: ";
	int n = 0;
	cin >> n;
	return n;
}
Course* getCourses(int crsCount)
{
	Course* courses = NULL;
	courses = new Course[crsCount];
	string courseName;
	int test1, test2, exam;

	for (int i = 0; i < crsCount; i++)
	{
		cout << i << ". Enter course name: ";
		cin >> courseName;
		cout << i << ". Enter test1, test2 and exam:\n";
		cin >> test1 >> test2 >> exam;
		courses[i].init(courseName, test1, test2, exam);
	}
	return courses;
}