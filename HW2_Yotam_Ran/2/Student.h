#pragma once

#include <iostream>
#include <string>
#include "Course.h"
using namespace std;

class Student
{
private:
	string _name;
	int _crsCount;
	Course* _courses;

public:
	void init(string name, Course** courses, int crsCount);
	string getName();
	void setName(string name);
	double getAvg();
	int getCrsCount();
	Course** getCourses();
	void resetStudent();
};

