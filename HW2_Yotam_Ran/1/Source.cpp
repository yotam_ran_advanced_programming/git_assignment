#include "myString.h"
using namespace std;

void main()
{
	myString str1;
	string input;
	cout << "Enter string: ";
	cin >> input;

	str1.init(input);														// init
	cout << "String: \"" << str1.getStr() << "\"\n";						// getStr
	cout << "Length: " << str1.getLength() << endl;							// getLength
	cout << "Char at 0: " <<str1.getCharUsingSquareBrackets(0) << endl;		// getCharUsingSquareBrackets
	cout << "Char at 0: " << str1.getCharUsingRoundBrackets(0) << endl;		// getCharUsingRoundBrackets
	cout << "First char: " << str1.getFirstChar() << endl;					// getFirstChar
	cout << "Last char: " << str1.getLastChar() << endl;					// getLastChar
	cout << "First o: " << str1.getCharFirstOccurrenceInd('o') << endl;		// getCharFirstOccurrenceInd
	cout << "Last l: " << str1.getCharLastOccurrenceInd('l') << endl;		// getCharLastOccurrenceInd
	cout << "First lo: " << str1.gstStringFirstOccurrenceInd("lo") << endl; // gstStringFirstOccurrenceInd
	cout << "Equal to \"lmao\": " << str1.isEqual("lmao") << endl;			// isEqual

	cout << endl;
	system("pause");
}