#include <iostream>
#include <string>

using std::string;

class myString
{
private:
	string* _strPtr;

public:
	void init(string str);
	string getStr();
	int getLength();
	char getCharUsingSquareBrackets(int index);
	char getCharUsingRoundBrackets(int index);
	char getFirstChar();
	char getLastChar();
	int getCharFirstOccurrenceInd(char c);
	int getCharLastOccurrenceInd(char c);
	int gstStringFirstOccurrenceInd(string str);
	bool isEqual(string s);
};
