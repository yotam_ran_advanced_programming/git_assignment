#include "myString.h"

using namespace std;

void myString::init(string str)
{
	_strPtr = new string; // Allocating new string.
	*_strPtr = str;
}

string myString::getStr()
{
	return *_strPtr;
}

int myString::getLength()
{
	return (*_strPtr).length();
}

char myString::getCharUsingSquareBrackets(int index)
{
	return (*_strPtr)[index];
}

char myString::getCharUsingRoundBrackets(int index)
{
	return (*_strPtr).at(index);
}

char myString::getFirstChar()
{
	return (*_strPtr).front();
}

char myString::getLastChar()
{
	return (*_strPtr).back();
}

int myString::getCharFirstOccurrenceInd(char c)
{
	return (*_strPtr).find_first_of(c);
}

int myString::getCharLastOccurrenceInd(char c)
{
	return (*_strPtr).find_last_of(c);
}

int myString::gstStringFirstOccurrenceInd(string str)
{
	return (*_strPtr).find(str);
}

bool myString::isEqual(string s)
{
	return (*_strPtr).compare(s) == 0 ? true : false;
}